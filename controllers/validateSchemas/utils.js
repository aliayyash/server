const Joi = require('joi');
const {requestValidator} = require('lumeos_middlewares');

class UtilsValidate {

    get can_create_polls() {
        return requestValidator(Joi.object().keys({
            isCommunity: Joi.boolean().required(),
        }))
    }

    get update_eos_account() {
        return requestValidator(Joi.object().keys({
            eos: Joi.string().allow(''), // '' means remove
        }))
    }

    get get_cost_to_create_polls() {
        return requestValidator(Joi.object().keys({
        }))
    }

    get get_reward_to_answer_polls() {
        return requestValidator(Joi.object().keys({
        }))
    }

    get cash_out_lume() {
        return requestValidator(Joi.object().keys({
            amount: Joi.number().integer().required(),
        }))
    }
}

module.exports = new UtilsValidate();
