const Joi = require('joi');
const {requestValidator} = require('lumeos_middlewares');

class CommentsValidate {

    get list() {
        return requestValidator(Joi.object().keys({
            id: Joi.number().integer().required(),
            isCommunity: Joi.boolean().required(),
       }))
    }

    get create() {
        return requestValidator(Joi.object().keys({
            id: Joi.number().integer().required(),
            parentId: Joi.number().integer().optional(),
            body: Joi.string().required(),
            isCommunity: Joi.boolean().required(),
        }));
    }

    get update() {
        return requestValidator(Joi.object().keys({
            id: Joi.number().integer().required(),
            liked: Joi.number().integer().required(),
        }))
    }

    get delete() {
        return requestValidator(Joi.object().keys({
            id: Joi.number().integer().required(),
        }))
    }

}

module.exports = new CommentsValidate();
