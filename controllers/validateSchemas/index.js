exports.communityValidate = require('./community');
exports.commentsValidate = require('./comments');
exports.usersValidate = require('./users');
exports.utilsValidate = require('./utils');
exports.communityPollsValidate = require('./communityPolls');
exports.communityPollAnswersValidate = require('./communityPollAnswers');