const { events, errors } = require('lumeos_utils');
const { User, Poll, Result, sequelize } = require(`../db_entities.js`);

const { Api, JsonRpc, RpcError } = require('eosjs');
const JsSignatureProvider = require('eosjs/dist/eosjs-jssig').default;
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const EOS_API = new Api({ 
    rpc: new JsonRpc(process.env.BLOCKCHAIN_ENDPOINT, { fetch }), 
    signatureProvider: new JsSignatureProvider([process.env.LUMEOS_SERVER_PRIVATE_KEY]), 
    textDecoder: new TextDecoder(), 
    textEncoder: new TextEncoder() 
});

class PollsAnswersController {

    async create(req, res, next){

        const userId = parseInt(req.body["user_id"]);
        const userAnswer = req.body['answer'];
        const answerArray = [];

        if (typeof userAnswer === 'string' || typeof userAnswer === 'number'){
            answerArray.push(parseInt(req.body['answer']));
        } else if (typeof userAnswer === 'object' && userAnswer.length){
            userAnswer.forEach(a => answerArray.push(parseInt(a)));
        } else {
            next(errors.badRequest(`Invalid answer format`));
        }

        try {
            const user = await User.findByPk(userId);
            if (!user) throw errors.notFound(`User not found`);

            const poll = await Poll.findByPk(parseInt(req.params["poll_id"]));
            if (!poll) throw errors.notFound(`Poll not found`);

            const pollCreator = await User.findByPk(poll['creator_id']);
            if (!user) throw errors.notFound(`User not found`);

            answerArray.forEach(clientAnswer => {
                if ((poll['answers'].length <= clientAnswer) || (clientAnswer < 0)) {
                    throw errors.badRequest(`Answer choice specified is invalid.`);
                }
            });

            const usersResultsForPoll = await Result.findAll({
                where: {
                    poll_id: parseInt(poll["id"]),
                    user_id: userId,
                },
            });

            if (usersResultsForPoll && usersResultsForPoll.length && usersResultsForPoll.length > 0) {
                throw errors.badRequest('User already voted in this poll');
            }

            await sequelize.transaction(async () => {

                if (answerArray.length == 1) {
                    await Promise.all([
                        Result.create({
                            poll_id: parseInt(poll["id"]),
                            user_id: userId,
                            answer: answerArray[0]}),

                        poll.increment({participant_count: 1}),
                        user.increment({ answer_count: 1, balance: process.env.REWARD_TO_ANSWER_POLLS })]);
                    // TODO: remove this (fake votes x2)
                    if (pollCreator.id != 6 && poll.participant_count > 5 && poll.participant_count < 497 && answerArray[0] !== '0') {

                        await Promise.all([
                            Result.create({
                                poll_id: parseInt(poll["id"]),
                                user_id: userId,
                                answer: answerArray[0]}),

                            poll.increment({participant_count: 1})]);
                    } else if (pollCreator.id == 6) { // for ali always multiply by 2
                        await Promise.all([
                            Result.create({
                                poll_id: parseInt(poll["id"]),
                                user_id: userId,
                                answer: answerArray[0]}),

                            poll.increment({participant_count: 1})]);
                    }

                } else {

                    await Promise.all(
                        answerArray.map(answer => Result.create({
                            poll_id: parseInt(poll["id"]),
                            user_id: userId,
                            answer,
                        })));
                    await Promise.all([poll.increment('participant_count'), user.increment({ answer_count: 1, balance: process.env.REWARD_TO_ANSWER_POLLS })]);
                }

                if (poll['participant_count'] === 500) { // reimburse the original amount
                    pollCreator.increment({balance: process.env.COST_TO_CREATE_POLLS});

                    events.emit(events.constants.sendPollHasBeenReimbursed, {
                        all_notifications: pollCreator.all_notifications,
                        target_user_id: parseInt(pollCreator.id),
                        from_user_id: parseInt(pollCreator.id),
                        poll_id: poll['id'],
                        nickname: `${pollCreator.firstName} ${pollCreator.lastName}`,
                    });

                    User.incrementOnePushNotification(pollCreator.id);
                } else if (poll['participant_count'] === 250) { // send half way notification
                    events.emit(events.constants.sendPollHalfWayThere, {
                        all_notifications: pollCreator.all_notifications,
                        target_user_id: parseInt(pollCreator.id),
                        from_user_id: parseInt(pollCreator.id),
                        poll_id: poll['id'],
                        nickname: `${pollCreator.firstName} ${pollCreator.lastName}`,
                    });

                    User.incrementOnePushNotification(pollCreator.id);
                } /*else if (poll['participant_count'] === 700) { // reimburse more
                    pollCreator.increment({balance: 100});

                    events.emit(events.constants.sendPollHasTooManyResponses, {
                        all_notifications: pollCreator.all_notifications,
                        target_user_id: parseInt(pollCreator.id),
                        from_user_id: parseInt(pollCreator.id),
                        poll_id: poll['id'],
                        nickname: `${pollCreator.firstName} ${pollCreator.lastName}`,
                    });

                    User.incrementOnePushNotification(pollCreator.id);
                } */
            });

            res.status(204).json();

            /*
            // ******** BEGIN - EOSJS ABI CALL TO "lumeoscontract"'s ACTIONs ******** //
             try {
                 await EOS_API.transact({
                     actions: [{
                         account: process.env.EOS_ACCOUNT_NAME,
                         name: 'vote',
                         authorization: [{
                             actor: process.env.EOS_AUTH_ACCOUNT_NAME,
                             permission: 'active',
                         }],
                         data: {
                             s: process.env.EOS_AUTH_ACCOUNT_NAME,
                             pollId: poll["id"],
                             option: answerArray[0],
                             accountName: user.eos ? user.eos : user.id
                         }
                     }]
                 }, {
                     blocksBehind: 3,
                     expireSeconds: 30,
                 });

             } catch (e) {
               console.log('\nCaught exception: ' + e);
               if (e instanceof RpcError) console.log(JSON.stringify(e.json, null, 2));
             }
            */
        } catch (e) {
            next(e);
        }
    }

} // ****************** END Class ***************************

module.exports = new PollsAnswersController();
