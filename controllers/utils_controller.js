const {users,polls,sequelize} = require('lumeos_models');
const {model, errors} = require('lumeos_utils');
const db_entities = require("../db_entities.js");
const Poll = db_entities.Poll;
var sanitizeHtml = require('sanitize-html');

class UtilsController {

    can_create_polls(req, res, next) {
        const valid_ids = [6,21,85,686,1892,3473,4522,5011,6108,6158,6142,5753,9760];

        const user_id = req.auth.user_id;
        const is_community = req.query.isCommunity;

        if (valid_ids.includes(user_id)) {
            return res.sendResponse({canCreate: true})
        } else {
            return users.isUserVerified({}, {user_id, is_community})
                .then((data) => {
                    if(data && data.length>0) {
                        polls.onePollADay({}, {user_id})
                            .then((d) => {
                                if(d && d.length>0) {
                                    res.sendResponse({canCreate: false});
                                } else {
                                    res.sendResponse({canCreate: true});
                                }
                            })
                            .catch(next);
                    } else {
                        res.sendResponse({canCreate: false});
                    }
                })
                .catch(next);
            }
    }

    update_eos_account(req, res, next) {
        const user_id = req.auth.user_id;
        const eos = req.body.eos || '';
        return users.updateEOSAccount({}, {user_id, eos})
            .then((data) => {
                res.sendResponse({updated: true});
            })
            .catch(next);
    }

    get_cost_to_create_polls(req, res, next) {
        const user_id = req.auth.user_id;
        // TODO: if the user have been creating lots of polls, increase price
        return res.sendResponse({price: process.env.COST_TO_CREATE_POLLS});
    }

    get_reward_to_answer_polls(req, res, next) {
        return res.sendResponse({price: process.env.REWARD_TO_ANSWER_POLLS});
    }

    cash_out_lume(req, res, next) {
        const user_id = req.auth.user_id;
        return res.sendResponse({done: false, message: 'This feature will be enabled soon!'});
        //return res.sendResponse({done: true, message: 'You successfully been credited 500 LUME to you eos account! Come back tomorrow to withdraw more!'});
        /* TODO: fill cashoutlume
        return users.cashOutLume({}, {user_id})
            .then((data) => {

                res.sendResponse({done: true});
            })
            .catch(next);*/
    }

}

module.exports = new UtilsController();
