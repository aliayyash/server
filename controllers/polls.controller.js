const { errors } = require('lumeos_utils');
const { Poll, User, Result } = require('../db_entities.js');

class PollsController {

    async listPollsUserInvolvedIn(req, res, next){

        const userId = parseInt(req.query['queryInvolved']);

        try {

            const user = await User.findByPk(userId);
            if(!user) throw errors.notFound(`User not found`);

            const pollsCreatedByUser = (await Poll.findAll({
                where: {
                    creator_id: userId,
                },
            })).map(pollEntity => pollEntity.id);

            const pollsUserParticipatedIn = (await Result.findAll({
                where: {
                    user_id: userId,
                },
            })).map(answerEntity => answerEntity.poll_id);

            const pollUserInvolvedId = await Poll.findAll({
                where: {
                    id: [...pollsCreatedByUser, ...pollsUserParticipatedIn],
                },
            });

            res.sendResponse(pollUserInvolvedId);

        } catch (e) {
            next(e);
        }

    }
}

module.exports = new PollsController();
