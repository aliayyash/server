module.exports = {

    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                { tableName: 'notifications'},
                'poll_id',
                { type: Sequelize.INTEGER },
            ),     
        ]);
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'notifications' },
            'poll_id',
        ),
    ]),
};
