module.exports = {

    up: async (queryInterface, Sequelize) => {

        const tableDefinition = await queryInterface.describeTable('transactions');

        if (!tableDefinition.createdAt){
            return Promise.all([
                queryInterface.addColumn(
                    { tableName: 'transactions' },
                    'createdAt',
                    { type: Sequelize.DATE },
                ),

                queryInterface.addColumn(
                    { tableName: 'transactions' },
                    'updatedAt',
                    { type: Sequelize.DATE },
                ),
            ]);
        }
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'transactions' },
            'createdAt',
        ),

        queryInterface.removeColumn(
            { tableName: 'transactions' },
            'updatedAt',
        ),
    ]),
};
