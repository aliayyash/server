'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.addIndex(`transactions`, {
			fields: [`poll_id`, `user_id`],
			unique: false,
		});
	},

	down: (queryInterface) => {
    return queryInterface.removeIndex(`transactions`, {
      fields: [`poll_id`, `user_id`],
    });
	}
};
