'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('surveys', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            payload: Sequelize.STRING,
            type: Sequelize.STRING,  // {zectr}
            createdAt: {type: Sequelize.DATE},
            updatedAt: {type: Sequelize.DATE},
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('zectr', {});
    }
};