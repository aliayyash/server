module.exports = {

    up: async (queryInterface, Sequelize) => {

        const tableDefinition = await queryInterface.describeTable('results');

        if (!tableDefinition.id){
            return Promise.all([
                queryInterface.addColumn(
                    { tableName: 'results' },
                    'id',
                    { type: Sequelize.INTEGER, unique: true, autoIncrement: true },
                ),

                queryInterface.addColumn(
                    { tableName: 'results' },
                    'createdAt',
                    { type: Sequelize.DATE },
                ),

                queryInterface.addColumn(
                    { tableName: 'results' },
                    'updatedAt',
                    { type: Sequelize.DATE },
                ),
            ]);
        }
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'results' },
            'id',
        ),

        queryInterface.removeColumn(
            { tableName: 'results' },
            'createdAt',
        ),

        queryInterface.removeColumn(
            { tableName: 'results' },
            'updatedAt',
        ),
    ]),
};
