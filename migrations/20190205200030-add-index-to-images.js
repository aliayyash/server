'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		queryInterface.addIndex(`images`, {
			fields: [`userId`],
			unique: false,
		});
		return queryInterface.addIndex(`images`, {
			fields: [`entityType`, `entityId`],
			unique: false,
		});
	},

	down: (queryInterface) => {
    queryInterface.removeIndex(`images`, `userId`);
    return queryInterface.removeIndex(`images`, {
			fields: [`entityType`, `entityId`],
		});
	}
};
