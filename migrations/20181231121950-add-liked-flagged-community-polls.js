module.exports = {

    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                { tableName: 'polls', schema: 'communities'},
                'liked_by',
                {type: Sequelize.ARRAY(Sequelize.INTEGER), defaultValue: []},
            ),
            queryInterface.addColumn(
                { tableName: 'polls', schema: 'communities'},
                'flagged_by',
                {type: Sequelize.ARRAY(Sequelize.INTEGER), defaultValue: []},
            ),            
        ]);
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'polls', schema: 'communities' },
            'liked_by',
        ),
        queryInterface.removeColumn(
            { tableName: 'polls', schema: 'communities' },
            'flagged_by',
        ),
    ]),
};
