module.exports = {

    up: async (queryInterface, Sequelize) => {

        const tableDefinition = await queryInterface.describeTable('profile_images');

        if (!tableDefinition.createdAt){
            return Promise.all([
                queryInterface.addColumn(
                    { tableName: 'profile_images' },
                    'createdAt',
                    { type: Sequelize.DATE },
                ),

                queryInterface.addColumn(
                    { tableName: 'profile_images' },
                    'updatedAt',
                    { type: Sequelize.DATE },
                ),
            ]);
        }
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'profile_images' },
            'createdAt',
        ),

        queryInterface.removeColumn(
            { tableName: 'profile_images' },
            'updatedAt',
        ),
    ]),
};
