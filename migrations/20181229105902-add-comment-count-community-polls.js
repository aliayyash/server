module.exports = {

    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                { tableName: 'polls', schema: 'communities'},
                'comment_count',
                { type: Sequelize.INTEGER, defaultValue: 0 },
            ),
            queryInterface.addColumn(
                { tableName: 'polls', schema: 'communities'},
                'status',
                { type: Sequelize.INTEGER, defaultValue: 0 },  // -1: INACTIVE, 0: ACTIVE, 1: FEATURED
            ),
            queryInterface.addColumn(
                { tableName: 'polls', schema: 'communities' },
                'votes',
                { type: Sequelize.INTEGER, defaultValue: 0 },
            ),                
        ]);
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'polls', schema: 'communities' },
            'comment_count',
        ),
        queryInterface.removeColumn(
            { tableName: 'polls', schema: 'communities' },
            'status',
        ),
        queryInterface.removeColumn(
            { tableName: 'polls', schema: 'communities' },
            'votes',
        ),

    ]),
};
