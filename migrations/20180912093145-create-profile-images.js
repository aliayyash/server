'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('profile_images', {
            user_id: {
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            image: {
                type: Sequelize.STRING,
            },
            createdAt: {type: Sequelize.DATE},
            updatedAt: {type: Sequelize.DATE}
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('profile_images', {});
    }
};