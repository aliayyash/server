'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
    queryInterface.addIndex(`results`, {
      fields: [`poll_id`],
      unique: false,
    });
    queryInterface.addIndex(`results`, {
      fields: [`user_id`],
      unique: false,
    });
		return queryInterface.addIndex(`results`, {
			fields: [`poll_id`, `user_id`],
			unique: false,
		});
	},

	down: (queryInterface) => {
    queryInterface.removeIndex(`results`, `poll_id`);
    queryInterface.removeIndex(`results`, `user_id`);
    return queryInterface.removeIndex(`results`, {fields: [`poll_id`, `user_id`]});
	}
};
