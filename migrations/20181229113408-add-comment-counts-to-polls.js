module.exports = {

    up: async (queryInterface, Sequelize) => {

        const tableDefinition = await queryInterface.describeTable('polls');

        if (!tableDefinition.status){
            return Promise.all([
                queryInterface.addColumn(
                    { tableName: 'polls' },
                    'comment_count',
                    { type: Sequelize.INTEGER, defaultValue: 0 },
                ),
                queryInterface.addColumn(
                    { tableName: 'polls' },
                    'status',
                    { type: Sequelize.INTEGER, defaultValue: 0 },  // -1: INACTIVE, 0: ACTIVE, 1: FEATURED
                ),
                queryInterface.addColumn(
                    { tableName: 'polls' },
                    'votes',
                    { type: Sequelize.INTEGER, defaultValue: 0 },
                ),                
            ]);
        }
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'polls' },
            'comment_count',
        ),
        queryInterface.removeColumn(
            { tableName: 'polls' },
            'status',
        ),
        queryInterface.removeColumn(
            { tableName: 'polls' },
            'votes',
        ),
    ]),
};
