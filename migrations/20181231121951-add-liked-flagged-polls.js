module.exports = {

    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                { tableName: 'polls'},
                'liked_by',
                {type: Sequelize.ARRAY(Sequelize.INTEGER), defaultValue: []},
            ),
            queryInterface.addColumn(
                { tableName: 'polls'},
                'flagged_by',
                {type: Sequelize.ARRAY(Sequelize.INTEGER), defaultValue: []},
            ),            
        ]);
    },

    down: queryInterface => Promise.all([
        queryInterface.removeColumn(
            { tableName: 'polls' },
            'liked_by',
        ),
        queryInterface.removeColumn(
            { tableName: 'polls' },
            'flagged_by',
        ),
    ]),
};
