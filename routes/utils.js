const express = require('express');
const {auth} = require('lumeos_middlewares');
const {utilsController} = require('lumeos_controllers');
const {utilsValidate} = require('../controllers/validateSchemas');

const router = express.Router();

router.route('/can_create_polls')
    .all(auth)
    .get(utilsValidate.can_create_polls, utilsController.can_create_polls);  // {isCommunity:true/false}

router.route('/update_eos_user')
    .all(auth)
    .post(utilsValidate.update_eos_account, utilsController.update_eos_account);  // {eos:eos_account_name}

router.route('/get_cost_to_create_polls')
    .all(auth)
    .get(utilsValidate.get_cost_to_create_polls, utilsController.get_cost_to_create_polls);

router.route('/get_reward_to_answer_polls')
    .all(auth)
    .get(utilsValidate.get_reward_to_answer_polls, utilsController.get_reward_to_answer_polls);

router.route('/cash_out_lume')
    .all(auth)
    .get(utilsValidate.cash_out_lume, utilsController.cash_out_lume);

module.exports = router;
