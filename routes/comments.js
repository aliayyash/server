const express = require('express');
const {auth} = require('lumeos_middlewares');
const {commentsController} = require('lumeos_controllers');
const {commentsValidate} = require('../controllers/validateSchemas');

const router = express.Router();

router.route('/comments/:id')
    .all(auth)
    .post(commentsValidate.create, commentsController.create)   // id=poll_id , {parentId, body, isCommunity}
    .get(commentsValidate.list, commentsController.list)        // id=poll_id , {isCommunity} -> return {commentsList};
    .delete(commentsValidate.delete, commentsController.delete) // id=comment_id , {isCommunity}
    .put(commentsValidate.update, commentsController.update);   // id=comment_id, {liked: 0, 1, -1}

module.exports = router;

/** CommentsList:
Comment {
  parentId: 0
  commentId: 0
  liked: true (if user +1 or -1 comment) {0, 1, -1}
  childrenCount: 0
  user: {
    firstName
    lastName
    profileImage
  }
  children: [
    Comment
  ]
}
*/
