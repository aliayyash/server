'use strict';

module.exports = (sequelize, DataTypes) => {
    const Surveys = sequelize.define('surveys', {
        payload: DataTypes.STRING,
        type: DataTypes.STRING,
    }); 
  
    return Surveys;
};
