'use strict';

const bcrypt = require('bcrypt');
const {UploadS3Service} = require('lumeos_services');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const moment = require('moment');

module.exports = (sequelize, DataTypes) => {
    const Polls = sequelize.define('polls', {
        question: DataTypes.STRING,
        price: {type: DataTypes.DOUBLE, defaultValue: 0},
        participant_count: {type: DataTypes.INTEGER, defaultValue: 0},
        avatar: {
            type: DataTypes.STRING,
            get: function () {
                return UploadS3Service.getImage(this.getDataValue('avatar'));
            }
        },
        answers: {
            type: DataTypes.STRING,
            get: function () {
                if (this.getDataValue('answers')) return JSON.parse(this.getDataValue('answers'));
            },
            set: function (val) {
                return this.setDataValue('answers', JSON.stringify(val));
            }
        },
        tags: {
            type: DataTypes.STRING,
            get: function () {
                if (this.getDataValue('tags')) return JSON.parse(this.getDataValue('tags'));
            },
            set: function (val) {
                return this.setDataValue('tags', JSON.stringify(val));
            }
        },
        creator_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: new Date(),
        },
    }, {});
    Polls.associate = function (models) {

    };
    Polls.methods = (models, _, db) => {
        Polls.onePollADay = (query, {user_id}) => {
            return Polls
                .scope(['defaultScope'])
                .findAll({
                    where: {
                        creator_id: user_id,
                        createdAt: { [Op.gt]: moment().subtract(48, "hours") },
                    }, 
                    attributes: ['id'],
                });
        };
    };
    return Polls;
};