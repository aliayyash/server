'use strict';

module.exports = (sequelize, DataTypes) => {
    const Comments = sequelize.define('comments', {
        parent_id: DataTypes.INTEGER,
        poll_id: DataTypes.INTEGER,
        creator_id: DataTypes.INTEGER,
        body: DataTypes.STRING,
        status: DataTypes.INTEGER, //  ACTIVE=0, DELETED=1,
        liked_by: DataTypes.ARRAY(DataTypes.INTEGER),
        flagged_by: DataTypes.ARRAY(DataTypes.INTEGER),
        is_community: DataTypes.BOOLEAN,
    }); 
    Comments.associate = (models) => {
        Comments.belongsTo(models.users, {foreignKey: 'creator_id'});
        Comments.belongsTo(models.polls, {foreignKey: 'poll_id'});
    };
    Comments.methods = (models, _, db) => {
        Comments.getList = (query, {poll_id, user_id, is_community}) => {
            return Comments
                .scope(['defaultScope', 'relatedData'])
                .findAll({
                    where: {
                        poll_id: poll_id,
                        is_community: is_community
                    }, 
                    order: [
                        ['status', 'ASC'],
                        ['id', 'DESC']
                    ],
                    attributes: ['id', 'parent_id', 'poll_id', 'body', 'creator_id', 'liked_by', 'flagged_by', 'createdAt', 'status'],
                    limit: 150
                });
        };
    };
    Comments.formatter = (models, _) => {
        Comments.formatData = (data) => {
            return _.pick(data, ['id', 'parent_id', 'poll_id', 'body', 'creator_id', 'liked_by', 'flagged_by', 'createdAt', 'status']);
        };
        Comments.formatResponse = (auth_user_id, data) => {
            function list_to_tree(list, auth_user_id) {
                var map = {}, node, roots = [], i;
                for (i = 0; i < list.length; i += 1) {
                    list[i].children = [];  // initialize the children
                    list[i].likes = list[i].liked_by.length - (list[i].flagged_by ? list[i].flagged_by.length : 0);
                    if (list[i].liked_by.includes(auth_user_id)) {
                        list[i].liked = 1;
                    } else if (list[i].flagged_by && list[i].flagged_by.includes(auth_user_id)) {
                        list[i].liked = -1;
                    } else {
                        list[i].liked = 0;
                    }
                }

                list.sort(function(a, b) {
                    if (a.parent_id === b.parent_id && a.parent_id !== 0) {
                        return a.createdAt-b.createdAt;
                    } else {
                        return b.likes-a.likes;
                    }
                });
                //list.sort(function(a, b) {return a.parent_id-b.parent_id});

                for (i = 0; i < list.length; i += 1) {
                    map[list[i].id] = i;
                }

                for (i = 0; i < list.length; i += 1) {
                    node = list[i];
                    if (node.parent_id > 0) {
                        list[map[node.parent_id]].children.push(node);
                    } else {
                        roots.push(node);
                    }
                }

                /*
                for (i = 0; i < roots.length; i += 1) {
                    if (roots[i].status == 1 && roots[i].children.length == 0) {
                        roots.splice(i, 1);
                    }
                }*/        

                return roots;
            }

            if (data instanceof Array) {
                return list_to_tree(data.map(d => d.toJSON()), auth_user_id);
            }
            if (data.toJSON) {
                data = data.toJSON();
            }
            return data;
        };
    };
  Comments.scopes = (models, sequelize) => {
    Comments.addScope('relatedData', {
      include: [
        {
          model: models.users,
          include: [{
            model: models.profileImages,
            attributes: ['image']
          }],
          attributes: ['id', 'firstName', 'lastName']
        },
      ],
    })
  };
  
  return Comments;
};
