const schedule = require('node-schedule');
const {PollService} = require('lumeos_services');

class CronJob {

    constructor() {

        this.pollService = new PollService();

        this.timePush = process.env.CRON_TIME_PUSH_NOTIFICATION ||  '0 22 * * 6'; //1=monday, 6=Saturday. 22=GMT

        this.pushNotificationsJob();
    }

    pushNotificationsJob() {
        this.jobPush = schedule.scheduleJob(this.timePush, () => {
            this.pollService.getNotAnswersPull()
        });
    }
}

module.exports = new CronJob();