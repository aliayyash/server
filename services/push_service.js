const FCM = require('fcm-push');

/**
 * @class PushService
 * @final
 * @constructor
 */
class PushService {

    constructor(config) {
        // if (!process.env.FIREBASE_SERVER_SECRET) {
        //     throw new Error('Variable FIREBASE_SERVER_SECRET not exist');
        // }
        this.config = config;
        this.client = new FCM(process.env.FIREBASE_SERVER_SECRET, this.config);
        this.message = {
            to: 'registration_token_or_topics',
            data: {
                your_custom_data_key: 'your_custom_data_value'
            },
            notification: {
                title: 'Title of your push notification',
                body: 'Body of your push notification',
                sound: 'default',
                priority: 'high',
                lights: 'true',
                badge: 2
            }
        };

    }

    /**
     * Send push notification on mobile
     *
     * @param to
     * @param data
     * @param notification {title, body, sound, priority, lights, badge}
     * @this {PushService}
     * @returns {Promise}
     */
    send(to, data, notification) {
        return new Promise((resolve, reject) => {
            if (data) {
                Object.assign(this.message, {data});
            }
            if (notification) {
                Object.assign(this.message, {
                    notification: Object.assign(this.message.notification, notification)
                });
            }
            this.client.send(Object.assign(this.message, {to})).then((data) => {
                resolve(data);
            }).catch((err) => {
                console.error(`[push-service-error] token > ${JSON.stringify(to)}`, err);
                reject(new Error('Error send push notification'));
            });
        });
    }

    sendCommentedOnComment(to, nickname, description) {
        console.log('sendCommentedOnComment');
        return this.send(to, null, {
            title: 'Lumeos',
            body: `"${nickname}" "${description}"`,
            badge: 1,
            priority: 'high'
        })
    }



    sendPolls(to, nickname, data) {
        return this.send(to, data, {
            title: 'Answered',
            body: `"${nickname}" answered your question.`
        })
    }

    sendPollsResult(to, {nickname}, data) {
        return this.send(to, data, {
            title: 'Polls Result',
            body: `"${nickname}" purchased your poll results.`
        })
    }

    sendFollow(to, {nickname}, data) {
        return this.send(to, data, {
            title: 'Following',
            body: `"${nickname}" is following you.`
        })
    }


    // for cron job
    sendNotAnswersPoll(to, {count, badge}, data) {
        return this.send(to, data, {
            title: 'Polls',
            body: `You can earn more Lumeos tokens today as you have ${count} unanswered polls`,
            badge
        })
    }

    // for server.lumeos.io/web/notifications
    sendCustomNotifications(to, {title, body, badge}, data) {
        return this.send(to, data, {
            title: title,
            body: body,
            badge
        })
    }
}

module.exports = PushService;
