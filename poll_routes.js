const express = require('express');
const { check, validationResult, oneOf } = require('express-validator/check');
const {events} = require('lumeos_utils');

const Sequelize = require('sequelize');
const { communityPolls } = require('lumeos_models');
const Op = Sequelize.Op;
const { UploadService, ImagesService, UploadS3Service } = require('lumeos_services');
const { pollsAnswersController, pollsController } = require('lumeos_controllers');

const dbSetup = require("./db_setup.js");
const sequelize = dbSetup.dbInstance;

const db_entities = require("./db_entities.js");
const User = db_entities.User;
const Poll = db_entities.Poll;
const Result = db_entities.Result;
const Transaction = db_entities.Transaction;
const getProfileImage = db_entities.getProfileImage;
const getUserName = db_entities.getUserName;
const getUserEOSName = db_entities.getUserEOSName;
const Immutable = require('seamless-immutable');
const moment = require('moment');


const { Api, JsonRpc, RpcError } = require('eosjs');
const JsSignatureProvider = require('eosjs/dist/eosjs-jssig').default;
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const endpoint = process.env.BLOCKCHAIN_ENDPOINT;
let EOS_account_name = process.env.EOS_ACCOUNT_NAME;
let EOS_auth_account_name = process.env.EOS_AUTH_ACCOUNT_NAME;
const lumeosacctPrivateKey = process.env.LUMEOS_SERVER_PRIVATE_KEY;
const crypto = require('crypto');

// in prod we use postgress, which requires iLike to case insensitive.
// sqlite does not support iLike operator
const likeOp = (process.env.ENV_PRODUCTION && process.env.LUMEOS_SERVER_DB) ? Op.iLike : Op.like;

const pollRouter = express.Router();

const removeEmptyObj = (obj) => {
  if (obj) {
    Object.keys(obj.dataValues).forEach((key) => (obj.dataValues[key] == null) && delete obj.dataValues[key]);
  }
  return obj;
};

const signatureProvider = new JsSignatureProvider([lumeosacctPrivateKey]);

const rpc = new JsonRpc(endpoint, { fetch });
const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

const callEOS = async(action, data) => {
    try {
        return await api.transact({
          actions: [{
            account: EOS_account_name,
            name: action,
            authorization: [{
              actor: EOS_auth_account_name,
              permission: 'active',
            }],
            data: data
          }]
        }, {
          blocksBehind: 3,
          expireSeconds: 30,
        });
    } catch(e) {
        console.log('\nCaught exception: ' + e);
        if (e instanceof RpcError) console.log(JSON.stringify(e.json, null, 2));
    }
}

/******************** API '/polls' *********************/ 
// handles the POST request to create a new poll question
// call EOSIO ACTION 'createpolls'
pollRouter.post('/polls', UploadService.middleware('avatar'),
    [
        check("question").isLength({ max: 255 }).not().isEmpty().trim().withMessage("Field 'question' cannot be empty"),
        check("answers").isArray().withMessage("Field 'answers' must be an array."),
        check("tags").optional().isArray().withMessage("Field 'tags' must be an array."),
        check("creator_id").custom((value, { req }) => {
            if (typeof req.auth.user_id === "undefined") {
                throw new Error("Field 'creator_id' must be.");
            }
            req.body.creator_id = req.auth.user_id;
            return true;
        }),
    ],
    // how this server responds (res) to the "create poll" '/poll' http request (req) 
    (req, res) => {
        const validationErrors = validationResult(req);
        //error checking
        if (!validationErrors.isEmpty()) {
            return res.status(422).json({ errors: validationErrors.array() });
        }
        // handle the image sizing/cropping as part of creating the poll question (if applicable)
        sequelize.sync()
        .then(() => {return User.findByPk(req.auth.user_id);}).then((user) => {
            if (user) {
                return user.decrement("balance", { by: process.env.COST_TO_CREATE_POLLS });
            }
        })
        .then(() => UploadService.uploadCroppedAndOriginal(req.file)
            .then(({ cropped, original }) => {

                Object.assign(req.body, { avatar: cropped.file });
                Object.assign(req.body, { createdAt: moment().tz("Europe/London") });

                return Poll.build(req.body).save()
                .then((poll) => {
                    if (cropped.file && original.file) {
                        return Promise.all([poll, ImagesService.createImage({
                            userId: req.auth.user_id,
                            entityId: poll.id,
                            entityType: 'Poll',
                            name: 'PollImage',
                            imageUrl: UploadS3Service.getImage(cropped.file),
                            originalImageUrl: UploadS3Service.getImage(original.file),
                        })]);
                    } else {
                        return Promise.all([poll, {}]);
                    }
                })
                // save poll info
                .then(([poll]) => {
                    poll.setDataValue('poll_id', poll.id);
                    return res.json(poll);
                })
                .then(async() => {
                    // ******************** BEGIN SMART CONTRACT CODE ******************* //
                    eosUserName = await getUserEOSName(req.auth.user_id);
                    
                    await callEOS('addpoll', {
                        s: eosUserName,
                        pollId: poll["id"],
                        communityId: 0
                      });

                    // *************** END - SMART CONTRACT CODE ****************** //
                }).catch(error => {});
            }),
        )
        .catch(error => {
            console.log(error);
            res.status(500).json({
                error,
                message: "Some error.",
            });
        });

    });
/*******************************************************/


pollRouter.get('/polls/:id', (req, res) => {
    Poll.findOne({
        where: {
            id: parseInt(req.params["id"]),
        },
        attributes: [
            ["id", "poll_id"],
            "question",
            "answers",
            "tags",
            "participant_count",
            "comment_count",
            "price",
            "creator_id",
            "createdAt",
            "avatar",
            "flagged_by",
            "liked_by",
            "status"
        ],
    })
    .then((poll) => {
        if (poll) {
            ImagesService.getImagesForEntity('Poll', parseInt(req.params["id"]))
            .then((images) => {
                poll.dataValues["images"] = images;
                return getProfileImage(poll["creator_id"]);
            })
            .then((result) => {
                poll.dataValues["creator_image"] = result;

                if (req.query["isAnswered"]) {
                    poll.dataValues["is_answered"] = 0;
                    Result.findOne({
                        where: {
                            user_id: parseInt(req.query["isAnswered"]),
                            poll_id: parseInt(req.params["id"]),
                        },
                        attributes: ["poll_id"],
                    })
                    .then((isAnswered) => {
                        if (isAnswered) {
                            poll.dataValues["is_answered"] = 1;
                        }
                        return res.json(poll);
                    })
                    .catch((error) => {
                        console.log(`I guess this is a first vote ever? user_id: ${req.query["isAnswered"]}, poll_id: ${req.params["id"]}`);
                        console.log(error);
                        res.json(poll);
                    });
                } else if (req.query["isBought"]) {
                    poll.dataValues["is_bought"] = 0;
                    Transaction.findOne({
                        where: {
                            user_id: parseInt(req.query["isBought"]),
                            poll_id: parseInt(req.params["id"]),
                        },
                        attributes: ["poll_id"],
                    })
                    .then(transaction => {

                        if (transaction) {
                            poll.dataValues["is_bought"] = 1;
                        } else {
                            // if it has been 5 days since the poll was created, mark it as bought (to prevent more users to answer)
                            const A_WEEK_OLD = moment().clone().subtract(5, 'days').startOf('day');
                            if (moment(poll.createdAt).isBefore(A_WEEK_OLD)) {
                                poll.dataValues["is_bought"] = 1;
                            }
                        }
                        return res.json(poll);
                    })
                    .catch(error => {
                        console.log(`I guess this is a first vote ever? user_id:${req.query["isBought"]}, poll_id: " + ${req.params["id"]}`);
                        console.log(error);
                        res.json(poll);
                    });
                } else {
                    return res.json(poll);
                }
            });
        } else {
            return res.status(404)
            .json({
                error: "Not Found",
                message: "Poll not found",
            });
        }
    });
});


/********************* API '/polls' ********************/
// API to display all poll questions
pollRouter.get('/polls', (req, res, next) => {

    const where_params = [];
    const orderParams = [];
    let limit = 500;

    if (req.query["queryTag"] || req.query["queryQuestion"]) {
        //                 const creator_id = req.auth.user_id;  // only allow lumeos to be able to search forever
        // TODO: poll creator should be able to see their polls for 2-3 weeks.

        // allow all search, but limit feed to last 7 days

    } else if (req.query["queryCreator"]) {
        where_params.push({
            createdAt: { [Op.gt]: moment().subtract(60, "days") },
        });
    } else {
        where_params.push({
            createdAt: { [Op.gt]: moment().subtract(5, "days") },
        });
    }

    if (req.query["orderBy"]) {
        orderParams.push([sequelize.col(req.query["orderBy"]), 'DESC']);
    }

    if (req.query["limit"]) {
        if (req.query["limit"] == 7) {
            where_params.push({
                createdAt: { [Op.gt]: moment().subtract(7, "days") },
            });
            limit = 40;
        } else if (req.query["limit"] == 1) {
            where_params.push({
                createdAt: { [Op.gt]: moment().subtract(1, "days") },
            });
            limit = 40;
        } else {
            limit = req.query["limit"];
        }
    }

    if (req.query["queryCreator"]) {
        where_params.push({
            creator_id: req.query["queryCreator"],
        });
    }

    if (req.query["queryQuestion"] && !req.query["queryTag"]) {
        where_params.push({
            question: { [likeOp]: `%${req.query["queryQuestion"]}%` },
        });
    }
    if (req.query["queryTag"] && !req.query["queryQuestion"]) {
        where_params.push({
            tags: { [likeOp]: `%${req.query["queryTag"]}%` },
        });
    }

    if (req.query["queryQuestion"] && req.query["queryTag"]) {
        where_params.push({
            [Op.or]: {
                tags: { [likeOp]: `%${req.query["queryTag"]}%` },
                question: { [likeOp]: `%${req.query["queryQuestion"]}%` },
            },
        });
    }

    let where_object = {};
    const where_attributes = [["id", "poll_id"], "question", "answers", "tags", "participant_count", "comment_count", "price", "creator_id", "createdAt", "avatar", "liked_by", "flagged_by", "status"];
    
    if (req.query["queryParticipant"]) {
        Result.findAll({
            where: { user_id: parseInt(req.query["queryParticipant"]) },
            attributes: ["poll_id"],
        })
        .then((result) => {
            if (!Array.isArray(result) || !result.length) {
                res.json([]);
            } else {
                where_params.push({
                    id: { [Op.or]: result.map(x => x.dataValues["poll_id"]) },
                });
                where_object = { where: Object.assign({}, ...where_params) };
                where_object.attributes = where_attributes;

                Poll.findAll(where_object)
                .then((poll) => {
                    if (poll) {
                        Promise.all(poll.map(x => getProfileImage(x["creator_id"])))
                        .then((images) => {
                            poll.map((elem, index) => {
                                elem.dataValues["creator_image"] = images[index];
                                return elem;
                            });
                         })
                        .then(() => Promise.all(poll.map(x => getUserName(x["creator_id"]))))
                        .then((names) => {
                            poll.map((elem, index) => {
                                elem.dataValues["creator_name"] = names[index];
                                return elem;
                            });
                        })
                        .then(() =>{
                            res.json(poll);
                         });
                        
                    } else {
                        res.status(200)
                        .json([]);
                    }
                });
            }
        });
    }else if (req.query['queryInvolved']){
        return pollsController.listPollsUserInvolvedIn(req, res, next);
    } else {
        where_object = {
            where: Object.assign({}, ...where_params),
            order: orderParams,
            limit,
        };

        where_object.attributes = where_attributes;

        Poll.findAll(where_object)
        .then((poll) => {
            if (poll) {
                // TODO: This is bad, we get profile images for all, and only then filter, invert.
                Promise.all(poll.map(x => getProfileImage(x["creator_id"])))
                .then((images) => {
                    poll.map((elem, index) => {
                        elem.dataValues["creator_image"] = images[index];
                        return elem;
                    });
                 })
                .then(() => Promise.all(poll.map(x => getUserName(x["creator_id"]))))
                .then((names) => {
                    poll.map((elem, index) => {
                        elem.dataValues["creator_name"] = names[index];
                        return elem;
                    });
                })
                .then(() =>{

                    if (req.query["queryFeatured"] || req.query["isAnswered"]) {
                        const callerId = req.query["queryFeatured"] ? parseInt(req.query["queryFeatured"]) : parseInt(req.query["isAnswered"]);
                        Result.findAll({
                            where: { user_id: callerId },
                            attributes: ["poll_id"],
                        })
                        .then((participated) => {
                            if (!Array.isArray(participated) || !participated.length) {
                                participated = [];
                            }
                            participated = new Set(participated.map(x => x.dataValues["poll_id"]));
                            if (req.query["queryFeatured"]) {
                                // We queried all polls user participated
                                // sort polls by engagement

                                // TODO: if the poll was created in the last minute move it to the top
                                //var ONE_MINUTE_AGO = moment().clone().subtract(1, 'minute').startOf('minute');

                                poll.sort(function(a, b) {
                                    /*TODO look more into this when online (in beta db)
                                    if (moment(a.createdAt).isAfter(ONE_MINUTE_AGO)) {
                                        return 1;
                                    }
                                    */
                                    const engA = a.liked_by.length - a.flagged_by.length;
                                    const engB = b.liked_by.length - b.flagged_by.length;
                                    if (engB == engA) {
                                        return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
                                    } else {
                                        return engB - engA;
                                    }
                                });
                                poll = poll.filter(element => {
                                    if (participated.has(element.dataValues["poll_id"])) {
                                        element.dataValues["is_answered"] = 1;
                                    } else {
                                        element.dataValues["is_answered"] = 0;
                                    }
                                    return element;
                                });

                                // remove polls with low votes from list
                                poll = poll.filter(element => {
                                    if (element.liked_by.length - element.flagged_by.length >= -4) {
                                        return element;
                                    }
                                });

                                res.json(poll);
                            } else {
                                poll = poll.filter(element => {
                                    if (participated.has(element.dataValues["poll_id"])) {
                                        element.dataValues["is_answered"] = 1;
                                    } else {
                                        element.dataValues["is_answered"] = 0;
                                    }
                                    return element;
                                });
                                res.json(poll);
                            }
                        });
                    } else {
                        // sort user polls by newest first
                        poll.sort(function(a, b) {
                            return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
                        });

                        res.json(poll);
                    }
                });
            } else {
                res.status(404).json({
                    error: "Not Found",
                    message: "Poll not found",
                });
            }
        })
        .catch((error) => {
            res.status(404).json({
                error: "Not Found",
                message: `poll table doesn't exist: ${error}`,
            });
        });
    }
});
/******************************************************/


pollRouter.post('/polls/:poll_id',
    [
        oneOf([
            check("user_id").isInt().withMessage("Field 'user_id' must be an int."),
            check("answer").isInt().withMessage("Field 'answer' must be an int."),
        ], [
            check("user_id").isInt().withMessage("Field 'user_id' must be an int."),
            check("answer").isArray().withMessage("Field 'answer' must be an int."),
        ]),
    ],

    (req, res, next) => {
        const validationErrors = validationResult(req);

        if (!validationErrors.isEmpty()) {
            return res.status(422).json({ errors: validationErrors.array() });
        } else{
            next();
        }
    },

    pollsAnswersController.create,
);

/*********** API '/polls/:poll_id/results' **************/
// API to display poll results (graphical)
function getPoll(req) { 
    return (req.body.isCommunity) ? 
            communityPolls.findByPk(req.params.poll_id) : 
            Poll.findByPk(req.params.poll_id);
}

// update polls (like/dislike)
pollRouter.put('/polls/:poll_id', (req, res) => {  // {liked: 0, 1, -1} AND {is_community}
    sequelize.sync()
    .then(() => {
        getPoll(req)
        .then(async(poll) => {
            if (poll) {

                const creator_id = req.auth.user_id;
                const liked = req.body.liked; // 0, -1, +1

                let likes = poll.liked_by ? poll.liked_by : [];
                let flags = poll.flagged_by ? poll.flagged_by : [];

                if (liked == 1) {
                    if (likes.indexOf(creator_id) <= -1) likes.push(creator_id);
                    var i = flags.indexOf(creator_id);  if (i > -1) flags.splice(i, 1);
                } else if (liked == -1) {
                    if (flags.indexOf(creator_id) <= -1) flags.push(creator_id);
                    var j = likes.indexOf(creator_id);  if (j > -1) likes.splice(j, 1);
                } else {
                    var k = likes.indexOf(creator_id);  if (k > -1) likes.splice(k, 1);
                    var l = flags.indexOf(creator_id);  if (l > -1) flags.splice(l, 1);
                }

                poll.update(
                    {'liked_by': likes},
                    {'where': {'id': req.params.poll_id}}
                );
                poll.update(
                    {'flagged_by': flags},
                    {'where': {'id': req.params.poll_id}}
                );

                res.json({});

                if (liked === 1 && (likes.length-flags.length) %11 ===0) { // notify poll creator that their poll is being upvoted

                    const pollCreator = await User.findByPk(poll.creator_id);

                    events.emit(events.constants.sendUpvotedPoll, {
                        all_notifications: pollCreator.all_notifications,
                        target_user_id: parseInt(pollCreator.id),
                        from_user_id: parseInt(pollCreator.id),
                        poll_id: req.params.poll_id,
                        nickname: `${pollCreator.firstName} ${pollCreator.lastName}`,
                    });

                    User.incrementOnePushNotification(pollCreator.id);
                }

                callEOS('uppolllikes', {
                    pollId: poll["id"],
                    likesCount: likes.length,
                    dislikesCount: flags.length
                });

            } else { // if poll
                return res.status(404)
                .json({
                    error: "Not Found",
                    message: "Poll not found",
                });
            }
        })
        .catch((e) => {
            console.log(e);
            res.status(404)
            .json({
                error: "Not Found",
                message: "Poll table doesn't exist",
            });
        });
    });
});



pollRouter.post('/polls/:poll_id/results', (req, res) => {
    sequelize.sync()
    .then(() => {
        const pollId = parseInt(req.params["poll_id"]);

        Poll.findByPk(pollId)
        .then((poll) => {
            if (poll) {

                const temp = [];
                poll["answers"].forEach(element => {
                    temp.push([element, 0]);
                });

                Result.findAll({ where: { poll_id: pollId } })
                .then((results) => {
                    results.forEach(element => {
                        temp[parseInt(element["answer"])] = [temp[parseInt(element["answer"])][0], temp[parseInt(element["answer"])][1] + 1];
                    });
                })
                .then(() => {

                    User.findByPk(parseInt(req.body['user_id']))
                    .then((user) => {
                        if (user) {
                            Transaction.findOrCreate({
                                where: {
                                    poll_id: parseInt(poll["id"]),
                                    user_id: parseInt(req.body["user_id"]),
                                },
                            })
                            .spread((transaction, created) => {
                                if (created) {
                                    // have to check here, because its ok for user to see poll after he bought it, and have zero balance
                                    if (user["balance"] < poll["price"]) {
                                        res.status(404)
                                        .json({
                                            error: "Bad request",
                                            message: "Not enough assets to buy a poll.",
                                        });
                                        return;
                                    }
                                    user.decrement("balance", { by: poll["price"] });

                                }

                                // if building this fails, its fine, user not goint to charged for subsequent requirests
                                const answers = {};
                                temp.forEach(element => {
                                    answers[element[0]] = element[1];
                                });

                                res.json({
                                    poll_id: poll["id"],
                                    question: poll["question"],
                                    answers,
                                });

                            }); // Transaction
                        } else {
                            res.status(404)
                            .json({
                                error: "Not Found",
                                message: "User not found",
                            });
                        }
                    }); // User

                }); // Poll
            } else { // if poll
                res.status(404)
                .json({
                    error: "Not Found",
                    message: "Poll not found",
                });
            }
        })
        .catch(() => {
            res.status(404)
            .json({
                error: "Not Found",
                message: "Poll table doesn't exist",
            });
        });
    });
});


module.exports = pollRouter;
