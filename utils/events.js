const {EventEmitter} = require('events');
const {Notifications} = require('../db_entities');
const {PushService} = require('lumeos_services');

const constants = {
    sendCommentedOnPoll: 'event:create-comment-on-poll',
    sendCommentedOnComment: 'event:create-comment-on-comment',
    sendUpvotedPoll: 'event:upvoted-poll',
    sendPollHasTooManyResponses: 'event:poll-too-many',
    sendPollHasBeenReimbursed: 'event:poll-reimbursed',
    sendPollHalfWayThere: 'event:poll-half-way-there',

    sendAnswerForPoll: 'event:create-answer-for-poll',
    sendAnswerForPollCallback: 'event:create-answer-for-poll-callback',
    sendResultForPoll: 'event:send-result-for-poll',
    sendResultForPollCallback: 'event:send-result-for-poll-callback',
    sendFolloweeFromFollower: 'event:send-followee-from-follower',
    sendFolloweeFromFollowerCallback: 'event:send-followee-from-follower-callback',
    sendNotAnswersPoll: 'event:send-not-answers-poll',
    sendNotAnswersPollCallback: 'event:send-not-answers-poll-callback',
    sendCustomNotifications: 'event:send-custom-notifications',
    sendCustomNotificationsCallback: 'event:send-custom-notifications-callback',
    sendCustomNotificationsPush: 'event:send-custom-notifications-push',
    sendCustomNotificationsPushCallback: 'event:send-custom-notifications-push-callback',
};

class Events extends EventEmitter {

    constructor() {
        super();
        this.setMaxListeners(0);
        this.constants = constants;
        this.pushService = new PushService;

        this.on(constants.sendCommentedOnPoll, this.sendCommentedOnPoll.bind(this));
        this.on(constants.sendCommentedOnComment, this.sendCommentedOnComment.bind(this));
        this.on(constants.sendUpvotedPoll, this.sendUpvotedPoll.bind(this));
        this.on(constants.sendPollHasBeenReimbursed, this.sendPollHasBeenReimbursed.bind(this));
        this.on(constants.sendPollHasTooManyResponses, this.sendPollHasTooManyResponses.bind(this));
        this.on(constants.sendPollHalfWayThere, this.sendPollHalfWayThere.bind(this));

        this.on(constants.sendAnswerForPoll, this.sendAnswerForPoll.bind(this));
        this.on(constants.sendResultForPoll, this.sendResultForPoll.bind(this));
        this.on(constants.sendFolloweeFromFollower, this.sendFolloweeFromFollower.bind(this));
        this.on(constants.sendNotAnswersPoll, this.sendNotAnswersPoll.bind(this));
        this.on(constants.sendCustomNotifications, this.sendCustomNotifications.bind(this));
        this.on(constants.sendCustomNotificationsPush, this.sendCustomNotificationsPush.bind(this));
    }

    sendCommentedOnPoll({all_notifications, poll_id, target_user_id, from_user_id, nickname}) {
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` commented on your poll`,
                poll_id,
                type: constants.sendCommentedOnPoll
            }).catch((err) => {
                console.log(`[${constants.sendCommentedOnPoll}]`, err);
            });

            /* TODO: when you have internet connection
            this.pushService.sendPolls(target_user_id, nickname).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });*/
    }

    sendCommentedOnComment({all_notifications, poll_id, target_user_id, from_user_id, nickname}) {
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` replied to your comment`,
                poll_id,
                type: constants.sendCommentedOnComment
            }).catch((err) => {
                console.log(`[${constants.sendCommentedOnComment}]`, err);
            });

            this.pushService.sendCommentedOnComment(target_user_id, nickname, ` replied to your comment`).then((data) => {
                console.log(`[${constants.sendCommentedOnComment}]`, data);
            }).catch((err) => {
                console.log(`[${constants.sendCommentedOnComment}]`, err);
            });
    }

    sendUpvotedPoll({all_notifications, poll_id, target_user_id, from_user_id, nickname}) {
        if (all_notifications) {
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` Your Poll is getting lots of upvotes`,
                poll_id,
                type: constants.sendUpvotedPoll
            }).catch((err) => {
                console.log(`[${constants.sendUpvotedPoll}]`, err);
            });

            /* TODO: when you have internet connection
            this.pushService.sendPolls(target_user_id, nickname).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });*/
        }

    }

    sendPollHasTooManyResponses({all_notifications, poll_id, target_user_id, from_user_id, nickname}) {
        console.log('sendPollHasTooManyResponses ' + poll_id + " " + target_user_id  + " " + from_user_id + " " + nickname);
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` Your Poll just got 700 responses! You have been rewarded an extra 100 LUME points!`,
                poll_id,
                type: constants.sendPollHasTooManyResponses
            }).catch((err) => {
                console.log(`[${constants.sendPollHasTooManyResponses}]`, err);
            });

            /* TODO: when you have internet connection
            this.pushService.sendPolls(target_user_id, nickname).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });*/
    }



    sendPollHasBeenReimbursed({all_notifications, poll_id, target_user_id, from_user_id, nickname}) {
        console.log('sendPollHasBeenReimbursed ' + poll_id + " " + target_user_id  + " " + from_user_id + " " + nickname);
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` Your Poll just got 500 responses! You have been reimbursed the poll creation cost.`,
                poll_id,
                type: constants.sendPollHasBeenReimbursed
            }).catch((err) => {
                console.log(`[${constants.sendPollHasBeenReimbursed}]`, err);
            });

            /* TODO: when you have internet connection
            this.pushService.sendPolls(target_user_id, nickname).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });*/
    }

    sendPollHalfWayThere({all_notifications, poll_id, target_user_id, from_user_id, nickname}) {
        console.log('sendPollHalfWayThere ' + poll_id + " " + target_user_id  + " " + from_user_id + " " + nickname);
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` Your Poll just got 250 responses! You are half way to get reimbursed your poll creation cost.`,
                poll_id,
                type: constants.sendPollHalfWayThere
            }).catch((err) => {
                console.log(`[${constants.sendPollHalfWayThere}]`, err);
            });

            /* TODO: when you have internet connection
            this.pushService.sendPolls(target_user_id, nickname).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });*/

    }

    sendAnswerForPoll({all_notifications, target_user_id, from_user_id, nickname}) {
        if (all_notifications) {
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ` answered your question`,
                type: constants.sendAnswerForPoll
            }).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });

            this.pushService.sendPolls(target_user_id, nickname).then((data) => {
                this.emit(this.constants.sendAnswerForPollCallback, data);
            }).catch((err) => {
                console.log(`[${constants.sendAnswerForPoll}]`, err);
                this.emit(this.constants.sendAnswerForPollCallback, err);
            });
        }

    }

    sendResultForPoll({all_notifications, not_answers_notifications, target_user_id, from_user_id}) {
        if (
            all_notifications ||
            (all_notifications && !not_answers_notifications)
        ) {
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ' purchased your poll results',
                type: constants.sendResultForPoll
            }).then((data) => {
                this.emit(this.constants.sendResultForPollCallback, null, data);
            }).catch((err) => {
                console.log(`[${constants.sendResultForPoll}]`, err);
                this.emit(this.constants.sendResultForPollCallback, err);
            });
        }
        //TODO on the future
        // tokens.map((item) => {
        //     if (item.active) {
        //         pushService.sendPollsResult(item.token, {nickname}).then((data) => {
        //             this.emit(this.constants.sendResultForPollCallback, data);
        //         }).catch((err) => {
        //             console.log(`[${constants.sendResultForPollCallback}]`, err);
        //             this.emit(this.constants.sendResultForPollCallback, err);
        //         });
        //     }
        // });
    }

    sendFolloweeFromFollower(params) {
        const {
            all_notifications,
            not_answers_notifications,
            follows_you_notifications,
            target_user_id,
            from_user_id
        } = params;
        if (
            all_notifications
            || (all_notifications && !not_answers_notifications)
            || (!all_notifications && follows_you_notifications)
        ) {
            Notifications.create({
                target_user_id,
                from_user_id,
                description: ' is following you',
                type: constants.sendFolloweeFromFollower
            }).then((data) => {
                this.emit(this.constants.sendFolloweeFromFollowerCallback, null, data);
            }).catch((err) => {
                console.log(`[${constants.sendFolloweeFromFollower}]`, err);
                this.emit(this.constants.sendFolloweeFromFollowerCallback, err);
            });
        }

        //TODO on the future
        // tokens.map((item) => {
        //     if (item.active) {
        //         pushService.sendFollow(item.token, {nickname}).then((data) => {
        //             this.emit(this.constants.sendFolloweeFromFollowerCallback, data);
        //         }).catch((err) => {
        //             console.log(`[${constants.sendFolloweeFromFollowerCallback}]`, err);
        //             this.emit(this.constants.sendFolloweeFromFollowerCallback, err);
        //         });
        //     }
        // });
    }

    sendNotAnswersPoll({count, to, not_answers_notifications, user_id, count_notifications: badge}) {
        const self = this;
        Promise.all([
            Notifications.create({
                target_user_id: user_id,
                from_user_id: user_id,
                description: `You can earn more Lumeos tokens today as you have ${count} unanswered polls`,
                type: constants.sendNotAnswersPoll
            }),
            // TODO: self.pushService.sendNotAnswersPoll(to, {count, badge})
        ]).then((data) => {
            data.map(e => console.log(JSON.stringify(e)));
            self.emit(self.constants.sendNotAnswersPollCallback, null, data);
        }).catch((error) => {
            if (Object.keys(error).length) {
                console.log('[event-error] ', `\n${JSON.stringify(error)}\n`);
                self.emit(this.constants.sendNotAnswersPollCallback, error, null);
            }
        });
    }

    sendCustomNotifications({user_id, from_user_id, title, description}) {
        Notifications.create({
            target_user_id: user_id,
            from_user_id: from_user_id,
            description,
            type: constants.sendCustomNotifications
        })
        .then((data) => {
            this.emit(this.constants.sendCustomNotificationsCallback, null, data);
        })
        .catch((error) => {
            console.log('[event-error] ', error);
            this.emit(this.constants.sendCustomNotificationsCallback, error, null);
        });
    }

    sendCustomNotificationsPush({to, title, body, count_notifications: badge}) {
        this.pushService.sendCustomNotifications(to, {title, body, badge})
            .then((data) => {
                this.emit(this.constants.sendCustomNotificationsPushCallback, null, data);
            })
            .catch((error) => {
                console.log('[event-error] ', error);
                this.emit(this.constants.sendCustomNotificationsPushCallback, error, null);
            });
    }
}

exports.constants = constants;
module.exports = Events;